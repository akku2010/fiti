import { Component } from "@angular/core";
import { ViewController, NavParams, ToastController } from "ionic-angular";
import { ApiServiceProvider } from "../../../providers/api-service/api-service";

@Component({
    // selector: 'page-modal',
    template: `
    <div class="mainDiv">
        <div class="secondDiv">
        <div text-center>
            <h5>Dealer Permission</h5>
            </div>
            <ion-list>
                <ion-item>
                    <ion-label>Add Vehicle</ion-label>
                    <ion-toggle [(ngModel)]="dev_permission"></ion-toggle>
                </ion-item>
                <ion-item>
                    <ion-label>Add Customer</ion-label>
                    <ion-toggle [(ngModel)]="cust_permission"></ion-toggle>
                </ion-item>
            </ion-list>
            <ion-row>
                <ion-col col-6>
                    <button ion-button (click)="submit()" color="gpsc" block>Submit</button>
                </ion-col>
                <ion-col col-6>
                    <button ion-button (click)="dismiss()" color="grey" block>Dismiss</button>
                </ion-col>
            </ion-row>
        </div>
    </div>
    `,
    styles: [`
      
            .mainDiv {
                padding: 50px;
                height: 100%;
                background-color: rgba(0, 0, 0, 0.7);
            }

            .secondDiv {
                padding-top: 20px;
                border-radius: 18px;
                border: 2px solid black;
                background: white;
            }
        
    `]
})

export class DealerPermModalPage {
    dealerData: any;
    dev_permission: boolean = true;
    cust_permission: boolean = true;
    constructor(
        navParams: NavParams,
        private viewCtrl: ViewController,
        private apiCall: ApiServiceProvider,
        private toastCtrl: ToastController
    ) {
        this.dealerData = navParams.get('param');
        console.log("dealer's data: ", this.dealerData)
        if(this.dealerData.device_add_permission != undefined && this.dealerData.cust_add_permission != undefined) {
            this.dev_permission = this.dealerData.device_add_permission;
            this.cust_permission = this.dealerData.cust_add_permission
        }
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    submit() {
        console.log("changed values: ", this.dev_permission)
        console.log("changed values: ", this.cust_permission)
        var url = this.apiCall.mainUrl + "users/setDealerPermission";
        var payload = {
            "device_add_permission": this.dev_permission,
            "cust_add_permission": this.cust_permission,
            "uid": this.dealerData._id
        }
        this.apiCall.urlpasseswithdata(url, payload).subscribe(respData => {
            console.log("response data check: ", respData);
            if (respData.message) {
                this.toastCtrl.create({
                    message: respData.message,
                    duration: 1500,
                    position: 'middle'
                }).present();
                this.dismiss();
            } else {
                this.toastCtrl.create({
                    message: 'Something went wrong.. Please try after some time.',
                    duration: 1500,
                    position: 'middle'
                }).present();
                this.dismiss();
            }
        })
    }

}